---
title: "Projects"
hero_image: "hero.jpg"
nometadata: true
notags: true
noshare: true
nocomments: true
---

<a href="#games">Games</a> &bullet; <a href="#apps">Apps</a> &bullet; <a href="#websites">Websites</a> &bullet; <a href="#GameJams">Game Jam Games</a>
<h2 id="games">Games</h2>
<div class="project">
<a href="/img/apteekkarin_aikaan.webp"><img src="/img/apteekkarin_aikaan.webp" class="project-image" /></a>
	<h3>Apteekkarin aikaan</H3>
	<p>A game implemented under the Memora platform that tells players the history of the Museum of Finnish Book Pukstaavi with the help of small games.  <br />Created using <strong>Unity.</strong></strong><br/> <a href="https://play.google.com/store/apps/details?id=com.Memorandum.Memora&hl=fi&gl=US"><strong>Download from Play Store</strong> </a></p>
</div>
<div class="project">
<a href="/img/edueffect_havainnekuva.webp"><img src="/img/edueffect_havainnekuva.webp" class="project-image" /></a>
	<h3>EduEffect</H3>
	<p>A story and map based adventure game that introduces international visitors to the Finnish education system and its history.</p>
	<p>I created the story structure using <strong><a href="https://www.inklestudios.com/ink/">Ink narrative markup language</a></strong>. Player groups were created using <strong>Firebase Realtime Database and authentication</strong>. Groups allow players to open up different solutions to problems. Map was created using <strong>Mapbox Unity integration</strong>.</p>
</div>
<div class="project">
<a href="/img/balancelot_small.jpg"><img src="/img/balancelot_small.jpg" class="project-image" /></a>
	<h3>Balancelot</H3>
	<p>This was a school project where we made a game about balancing on an unicycle while overcoming obstacles. I was one of the two programmers on the project. <br />
	<strong>Steam:</strong> <a href="https://store.steampowered.com/app/1035850/Balancelot/">https://store.steampowered.com/app/1035850/Balancelot/</a></p>
	<span class="clear"></span>
</div>
<div class="project">
<a href="/img/432533-crop0_0_1603_587-e3ef3sap-v4.webp"><img src="/img/432533-crop0_0_1603_587-e3ef3sap-v4.webp" class="project-image" /></a>
	<h3>He Came Through the Door</H3>
	<p>I was gameplay programmer, composer and actor in the masterpiece adventure game.  <br />Created using <strong>Adventure Game Studio and Blender</strong>.<br />
	<strong>GameJolt:</strong> <a href="https://gamejolt.com/games/hctd/432533">https://gamejolt.com/games/hctd/432533</a></p>
	<span class="clear"></span>
</div>
<h2 id="apps">Apps</h2>
<div class="project">
<a href="/img/memora.webp"><img src="/img/memora.webp" class="project-image" /></a>
	<h3>Memora-platform</H3>
	<p>A map-based application for finding services and apps around users. Platform is meant to be used to provide an easy platform for sharing developed apps to users. No need to create new projects and publish multiple smaller applications. A subscription service for clients.  <br />Created using <strong>Unity, Android, Mapbox, Wordpress,
Woocommerce, PHP and REST API</strong><br/> <a href="https://play.google.com/store/apps/details?id=com.Memorandum.Memora&hl=fi&gl=US"><strong>Download from Play Store</strong> </a>
	<span class="clear"></span></p>
</div>
<div class="project">
<a href="/img/viikinlahti_campus.webp"><img src="/img/viikinlahti_campus.webp" class="project-image" /></a>
	<h3>Viikinlahti Campus</H3>
	<p>The Viikinlahti Campus app brings the company's communications and history into one application. The app allows visitors to receive notifications of events in the area as well as find more information and news. History is told through significant roads in the area.  <br />Created using <strong>Unity, Android, iOS, Mapbox, Node.js, Express.js, Strapi Headless CMS and Vue.js.</strong><br/> <a href="https://play.google.com/store/apps/details?id=com.Memorandum.Viikinlahti&hl=fi&gl=US"><strong>Download from Play Store</strong> </a>
	<span class="clear"></span></p>
</div>
<div class="project">
<a href="/img/valonkaupunki.webp"><img src="/img/valonkaupunki.webp" class="project-image" /></a>
	<h3>Valon kaupunki</H3>
	<p>A map-based application for finding all the illuminated objects in the event and their details.  <br />Created using <strong>Unity, Android, iOS and Mapbox</strong><br/> <a href="https://play.google.com/store/apps/details?id=com.Memorandum.Valonkaupunki&hl=fi&gl=US"><strong>Download from Play Store</strong> </a>
	<span class="clear"></span></p>
</div>

<div class="project">
<a href="/img/savonlinna_riesteilyt.webp"><img src="/img/savonlinna_riesteilyt.webp" class="project-image" /></a>
	<h3>Savonlinna-Risteilyt</H3>
	<p>Tells users about the locations and their history on the route of the cruise. Map-based application in three languages. A small attraction of the application is the AR Ringed seal created using the Unity AR Foundation, which makes the Ringed seal appear on the deck of the ship. <br />Created using <strong>Unity, Mapbox</strong> and <strong>AR Foundation.</strong><br/> <a href="https://play.google.com/store/apps/details?id=com.Memorandum.Savonlinna&hl=fi&gl=US"><strong>Download from Play Store</strong> </a>
	<span class="clear"></span></p>
</p>
</div>
<div class="project">
<a href="/img/tangomarkkinat.webp"><img src="/img/tangomarkkinat.webp" class="project-image" /></a>
	<h3>Seinäjoen Tangomarkkinat-app</H3>
	<p>An application that tells the history of the Seinäjoen Tangomarkkinat on a timeline. Also has the AR feature for taking a picture with a crown on the user’s head.<br />Created using <strong>Unity</strong> and <strong>AR Foundation.</strong><br/> <a href="https://play.google.com/store/apps/details?id=com.Memorandum.Tangomarkkinat&hl=fi&gl=US"><strong>Download from Play Store</strong> </a>
	<span class="clear"></span></p>
</div>
<div class="project">
<h2 id="GameJams">Game Jam Games</h2>
<div class="project">
	<img src="/img/SpaceDefender.gif" class="project-image" />
	<h4>Ludum Dare 42 (72 hours)</h4>
	<p>For my first Ludum Dare game I created a space shooter where spawning of enemies increases as time goes on. The theme was Running out of space.<br />
	<strong>Game:</strong> <a href="https://sambi.itch.io/space-defender-2000"> Itch.io</a><br />
	<strong>Source:</strong> <a href="https://gitlab.com/SamiAutio/ld42"> GitLab</a></p>
	<span class="clear"></span>
</div>
<h2 id="School">School projects</h2>
<div class="project">
<a href="/img/Aviatorum.webp"><img src="/img/Aviatorum.webp" class="project-image" /></a>
	<h3>Aviatorum</H3>
	<p>This was a physics based arcade game where you fight enemies to achieve highscores. I was one of the programmers on this project and did online leaderboards using <strong>Node.js</strong> and <strong>MongoDB</strong>. I also did sound design and sound programming using <strong>Wwise</strong>.<br />
	<strong>Game:</strong> <a href="https://aviatorum.itch.io/aviatorum"> Itch.io</a>
	<span class="clear"></span>
</div>
<h2 id="websites">Websites</h2>
<div class="project">
<a href="/img/kehrakumppanit.webp"><img src="/img/kehrakumppanit.webp" class="project-image" /></a>
	<h3>Kehräkumppanit ry</H3>
	<p>I created the template and new look for Kehräkumppanit ry's website.<br />
	<strong>URL:</strong> <a href="http://www.kehra.net/"> kehra.net</a>
	<span class="clear"></span>
</div>
<div class="project">
<img src="/img/sun_saarijarvi_small.png" class="project-image" />
	<h3>SUN Saarijärvi</H3>
	<p>SUN Saarijärvi's website is a place where local events are gathered in a one place. These pages were created using <strong>Wordpress</strong>. There was some heavy customization done to Events Manager and the theme was made for the site.<br /> Sadly this website has been closed.
	<span class="clear"></span>
</div>
<div class="project">
<img src="/img/kisakino_small.png" class="project-image" />
	<h3>Kisakino</H3>
	<p>Kisakino Cinema needed a website where to show currently running movies and movies that are coming. These pages were created using <strong>Wordpress</strong> and required quite a lot of customization.</p>
	<strong>URL:</strong> <a href="http://kisakino.fi/"> kisakino.fi</a>
	<span class="clear"></span>
</div>