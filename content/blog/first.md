---
title: "Welcome to my website!"
hero_image: "hero.jpg"
date: 2018-11-05T03:43:12+02:00
description: "About me and my projects."
draft: false
Section: blog
---

<h3>Here you will find information <a href="{{< ref "/" >}}">about me</a> and <a href="{{< ref "/projects" >}}">my projects</a></H3>