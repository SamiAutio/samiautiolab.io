---
title: "About"
hero_image: "hero.jpg"
nometadata: true
notags: true
noshare: true
nocomments: true
---

<p>Hi! I'm Sami, a web and game developer from central finland.</p>
<p>I have Bachelor's degree in Business Information Technology from JAMK University of Applied Sciences.</p>
<p>Main focus of my studies was in game development, but I've also studied databases and other web-related aspects of software development.</p>
<p>You can read more about projects I've been involved in <a href="{{< ref "/projects" >}}">here.</a></p>
<hr />
<p>Here are some of my main skills</p>
<strong>Game development</strong>
<ul>
    <li>Unity</li>
	<ul>
		<li>AR Foundation</li>
		<li>MapBox</li>
		<li>Firebase</li>
	</ul>
    <li>Programming Languages</li>
	<ul>
		<li>C#</li>
		<li>JavaScript</li>
	</ul>
</ul>
<strong>Web development</strong>
<ul>
    <li><b>Frontend:</b> Vue.js and React.js</li>
    <li><b>Backend:</b> NodeJS, PHP, MongoDB, MySQL (MariaDB) and Redis</li>
</ul>